package com.powellapps.carronovo;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;

import java.util.ArrayList;

import adapter.AdapterCarro;
import model.BancoDeDados;
import model.Carro;

public class ListaDeCarrosActivity extends AppCompatActivity {

    private ListView listViewCarros;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_de_carros);
        listViewCarros = (ListView) findViewById(R.id.listViewCarros);
        mostraCarros();
    }

    private void mostraCarros() {
        AdapterCarro adapterCarro = new AdapterCarro(this, BancoDeDados.getInstance().getCarros());
        listViewCarros.setAdapter(adapterCarro);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_lista_de_carros, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_novo) {
            startActivity(new Intent(getApplicationContext(), CarroNovoActivity.class));
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume(){
        super.onResume();
        mostraCarros();
    }
}
