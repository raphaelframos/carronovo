package com.powellapps.carronovo;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import java.util.List;


public class EntradaActivity extends ActionBarActivity {

    private static final int TEMPO_ENTRADA = 1000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_entrada);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent trocaDeActivity = new Intent(EntradaActivity.this, ListaDeCarrosActivity.class);
                startActivity(trocaDeActivity);
                finish();
            }
        }, TEMPO_ENTRADA);
    }
}
