package com.powellapps.carronovo;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import model.BancoDeDados;
import model.Carro;


public class CarroNovoActivity extends ActionBarActivity {

    private EditText editTextModelo;
    private Spinner spinnerMarca;
    private EditText editTextAno;
    private CheckBox checkBoxAbs;
    private CheckBox checkBoxArCondicionado;
    private CheckBox checkBoxSom;
    private CheckBox checkBoxAirBag;
    private Button buttonSalvar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_carro_novo);

        editTextModelo = (EditText) findViewById(R.id.editTextModelo);
        spinnerMarca = (Spinner) findViewById(R.id.spinnerMarca);
        editTextAno = (EditText) findViewById(R.id.editTextAno);
        spinnerMarca = (Spinner) findViewById(R.id.spinnerMarca);
        checkBoxAbs = (CheckBox) findViewById(R.id.checkBoxAbs);
        checkBoxAirBag = (CheckBox) findViewById(R.id.checkBoxAir);
        checkBoxArCondicionado = (CheckBox) findViewById(R.id.checkBoxAr);
        checkBoxSom = (CheckBox) findViewById(R.id.checkBoxSom);
        buttonSalvar = (Button) findViewById(R.id.buttonSalvar);

        editTextModelo.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(count > 0){
                    buttonSalvar.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        buttonSalvar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String modelo = editTextModelo.getText().toString();
                String ano = editTextAno.getText().toString();
                String marca = (String) spinnerMarca.getSelectedItem();
                boolean abs = checkBoxAbs.isChecked();
                boolean arCondicionado = checkBoxArCondicionado.isChecked();
                boolean som = checkBoxSom.isChecked();
                boolean airBag = checkBoxAirBag.isChecked();

                Carro novoCarro = new Carro();
                novoCarro.setAbs(abs);
                novoCarro.setAirbag(airBag);
                novoCarro.setAno(Integer.valueOf(ano));
                novoCarro.setMarca(marca);
                novoCarro.setModelo(modelo);
                novoCarro.setArCondicionado(arCondicionado);
                novoCarro.setSom(som);

                BancoDeDados.getInstance().adicionaCarro(novoCarro);
                limparTela();
             //   Intent intent = new Intent(CarroNovoActivity.this, CarrosActivity.class);
             //   intent.putExtra("carro", novoCarro);
              //  startActivity(intent);

            }
        });


    }

    private void limparTela(){
        editTextAno.setText("");
        editTextModelo.setText("");
        spinnerMarca.setSelection(0);
        checkBoxAbs.setChecked(false);
        checkBoxAirBag.setChecked(false);
        checkBoxArCondicionado.setChecked(false);
        checkBoxSom.setChecked(false);
        editTextModelo.requestFocus();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_carro_novo, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }else if(id == R.id.action_limpar){
            limparTela();
        }

        return super.onOptionsItemSelected(item);
    }
}
