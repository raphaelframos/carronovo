package adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.powellapps.carronovo.R;

import java.util.ArrayList;
import java.util.zip.Inflater;

import model.Carro;

/**
 * Created by raphael on 10/08/15.
 */
public class AdapterCarro extends ArrayAdapter<Carro> {

    private LayoutInflater inflater;
    private ArrayList<Carro> carros;

    public AdapterCarro(Context context, ArrayList<Carro> carros) {
        super(context, R.layout.adapter_carro, carros);
        inflater = LayoutInflater.from(context);
        this.carros = carros;
    }

    public View getView(int posicao, View view, ViewGroup parent){

        View linha = view;
        CarroViewHolder carroViewHolder = null;

        if(linha == null){
            linha = inflater.inflate(R.layout.adapter_carro,parent,false);
            carroViewHolder = new CarroViewHolder(linha);
            linha.setTag(carroViewHolder);
        }
        carroViewHolder = (CarroViewHolder) linha.getTag();

        Carro carro = carros.get(posicao);
        carroViewHolder.textViewModelo.setText(carro.getModelo());
        carroViewHolder.textMarca.setText(carro.getMarca());

        return linha;
    }

    public static class CarroViewHolder{
        TextView textViewModelo;
        TextView textMarca;

        public CarroViewHolder(View view) {
            textMarca = (TextView) view.findViewById(R.id.textViewMarca);
            textViewModelo = (TextView) view.findViewById(R.id.textViewModelo);
        }
    }
}
