package model;

import java.util.ArrayList;

/**
 * Created by raphael on 10/08/15.
 */
public class BancoDeDados {

    private static BancoDeDados instance;
    private ArrayList<Carro> carros;

    public BancoDeDados(){
        carros = new ArrayList<>();
    }

    public static BancoDeDados getInstance(){
        if(instance == null){
            instance = new BancoDeDados();
        }
        return instance;
    }


    public ArrayList<Carro> getCarros() {
        return carros;
    }

    public void setCarros(ArrayList<Carro> carros) {
        this.carros = carros;
    }

    public void adicionaCarro(Carro novoCarro) {
        this.carros.add(novoCarro);
    }
}
